module.exports = {
  plugins: ["vue"], // enable vue plugin
  extends: ["plugin:vue/essential", "airbnb-base", "prettier"] // activate vue related rules
};
