import axios from "axios";

export default {
  install: (Vue, uri = "api") => {
    Object.defineProperty(Vue.prototype, "$api", {
      value: axios.create({
        host: "localhost",
        baseURL: `http://localhost:3000/${uri}`,
        port: 3000
      })
    });
  }
};
